draw_ann = function(input_val, output_val, layer_val, per_layer_val)
{
  input_val     = Number(input_val);
  output_val    = Number(output_val);
  layer_val     = Number(layer_val);
  per_layer_val = Number(per_layer_val);

//  create an array with nodes
  var nodes = new vis.DataSet();

  for(i = 0; i < input_val + output_val + (layer_val*per_layer_val); i++)
  {
    nodes.add({id: i, label: i+''});
  }

  // create an array with edges
  var edges = new vis.DataSet();
  
  for(i = 0; i < per_layer_val; i++)
  {
    for(j = 0; j <  input_val; j++)
    {
      edges.add({from: j, to: input_val + i});
    }
  }
  
  for(i = 1; i < layer_val; i++)
  {
    for(j = 0; j < per_layer_val; j++)
    {
      for(k = 0; k < per_layer_val; k++)
      {
        edges.add({from: input_val + (i-1)*per_layer_val + k, to: input_val + i*per_layer_val + j});
      }
    }
  }
  
  for(j = 0; j < output_val; j++)
  {
    for(i = 0; i < per_layer_val; i++)
    {
      edges.add({from: input_val + (layer_val*per_layer_val) + j, to: input_val + (layer_val*per_layer_val) - per_layer_val + i});
    }
  }
  // create a network
  var container = document.getElementById('network');

  var data = { nodes: nodes, edges: edges };

  var options =
  {
    autoResize: true,
    height: '100%',
    width: '90%',
    physics:
    {
      forceAtlas2Based:
      {
        gravitationalConstant: -126,
        centralGravity: 0.005,
        springLength: 230,
        springConstant: 0.18,
        avoidOverlap: 3.5
      },
      maxVelocity: 1,
      solver: 'forceAtlas2Based',
      timestep: 0.35,
      stabilization:
      {
        enabled: true,
        iterations: 500,
        updateInterval: 25
      }
    },
    interaction:{hover:true}
  };

  var result = new vis.Network(container, data, options);
  
  result.on("doubleClick", function (params) { result.deleteSelected(); });
  
  update_form(input_val, output_val);
  
  return result;
}

update_form = function(input_val, output_val)
{
  $('#ann_input').empty();
  for(i = 0; i < input_val; i++)
  {
    $('#ann_input').append('<label><span style="width:45%;">Entrada ' + i + '</span><input class="input_data" id="input_' + i + '" type="text"/></label>');
  }
  
  $('#ann_input').append('<label style="margin-top:4px"><span style="width:45%;"></span><button type="button">Propagar</button></label>');
  
  $('#ann_output').empty();
  for(i = 0; i < output_val; i++)
  {
    $('#ann_output').append('<label><span style="width:45%;">Salida ' + i + '</span><input class="input_data" id="input_' + i + '" type="text" disabled/></label>');
  }
}

/*
<script type="text/javascript">

    // create an array with nodes
    var nodes = new vis.DataSet([
        {id: 1, label: 'Node 1', title: 'I have a popup!'},
        {id: 2, label: 'Node 2', title: 'I have a popup!'},
        {id: 3, label: 'Node 3', title: 'I have a popup!'},
        {id: 4, label: 'Node 4', title: 'I have a popup!'},
        {id: 5, label: 'Node 5', title: 'I have a popup!'}
    ]);

    // create an array with edges
    var edges = new vis.DataSet([
        {from: 1, to: 3},
        {from: 1, to: 2},
        {from: 2, to: 4},
        {from: 2, to: 5}
    ]);

    // create a network
    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {interaction:{hover:true}};
    var network = new vis.Network(container, data, options);

    network.on("click", function (params) {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
        console.log('click event, getNodeAt returns: ' + this.getNodeAt(params.pointer.DOM));
    });
    network.on("doubleClick", function (params) {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>doubleClick event:</h2>' + JSON.stringify(params, null, 4);
    });
    network.on("oncontext", function (params) {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>oncontext (right click) event:</h2>' + JSON.stringify(params, null, 4);
    });
    network.on("dragStart", function (params) {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>dragStart event:</h2>' + JSON.stringify(params, null, 4);
    });
    network.on("dragging", function (params) {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>dragging event:</h2>' + JSON.stringify(params, null, 4);
    });
    network.on("dragEnd", function (params) {
        params.event = "[original event]";
        document.getElementById('eventSpan').innerHTML = '<h2>dragEnd event:</h2>' + JSON.stringify(params, null, 4);
    });
    network.on("zoom", function (params) {
        document.getElementById('eventSpan').innerHTML = '<h2>zoom event:</h2>' + JSON.stringify(params, null, 4);
    });
    network.on("showPopup", function (params) {
        document.getElementById('eventSpan').innerHTML = '<h2>showPopup event: </h2>' + JSON.stringify(params, null, 4);
    });
    network.on("hidePopup", function () {
        console.log('hidePopup Event');
    });
    network.on("select", function (params) {
        console.log('select Event:', params);
    });
    network.on("selectNode", function (params) {
        console.log('selectNode Event:', params);
    });
    network.on("selectEdge", function (params) {
        console.log('selectEdge Event:', params);
    });
    network.on("deselectNode", function (params) {
        console.log('deselectNode Event:', params);
    });
    network.on("deselectEdge", function (params) {
        console.log('deselectEdge Event:', params);
    });
    network.on("hoverNode", function (params) {
        console.log('hoverNode Event:', params);
    });
    network.on("hoverEdge", function (params) {
        console.log('hoverEdge Event:', params);
    });
    network.on("blurNode", function (params) {
        console.log('blurNode Event:', params);
    });
    network.on("blurEdge", function (params) {
        console.log('blurEdge Event:', params);
    });


</script>

*/
